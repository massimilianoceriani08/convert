# Convert API
Simple web service that allows a currency exchange between two currency rates, values calulated at a given date.
It automatically retrieves the latest exhange info at `every request`.
Every push or merge on this repository triggers a gitlab pipeline. It automatically runs the test at every `push` and build and publish a docker image (https://hub.docker.com/repository/docker/massicer/convert) at every `merge request` or `push` that involves the master branch.

### How To start the service

### Docker mode
#### 1. pull the image
```
docker pull massicer/convert
```
#### 2. create container and expose the port 8080
```
docker run -p 8080:8080 massicer/convert
```

### Alternative Mode
#### 1. clone repo and install dependencies
```
npm install
```

#### 2. start the web service passing the ENV values during
```
EXCHANGE_FILE_URL={exchangeWebUrl} PORT={servicePort} npm start
```

# How to use service

Method: GET
Port: mapped port or env PORT value.
Path: /updateRates/performExchange'
Query Params required: 
  - `amount`: the amount to convert
  - `srcCurrency`; the source ISO currency of the amount
  - `destCurrency`: the destination ISO currency.
  - `referenceDate`: the date to use to get the currencies rates.

### Success Response:

Code: 200 
Content: 
```
{
    "amount": 1,
    "currency": "EUR"
}
```
### Error Response:

Code: 404 NOT FOUND 
Content: 
```
{ error : "error msg" }
```
