
const { query } = require('express-validator/check');

module.exports.performExchangeValidator = [
  query('amount', 'amount should be numeric').isNumeric(),
  query('srcCurrency', 'srcCurrency should max have 3 in length').isLength({ max: 3 }),
  query('destCurrency', 'destCurrency should max have 3 in length').isLength({ max: 3 }),
  query('referenceDate', 'referenceDate must be present'),
];
