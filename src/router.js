const { Router } = require('express');
const exchangeController = require('./controllers/exchange.controller');

const { handleValidationResult } = require('./middleware/validation.middleware');
const {
  performExchangeValidator,
} = require('./validators/ecxchange.validator.js');

const router = Router();

router.get('/updateRates/performExchange', performExchangeValidator, handleValidationResult, exchangeController.performExchange);

module.exports = router;
