
const bodyParser = require('body-parser');
const express = require('express');
const router = require('./router.js');

function checkRequiredEnvValues(envName) {
  if (process.env[envName] === undefined) throw new Error(`${envName} env value is required`);
}

const startServer = async () => {
  checkRequiredEnvValues('PORT');
  checkRequiredEnvValues('EXCHANGE_FILE_URL');

  const app = express();
  app.use(bodyParser.json());
  app.use(router);
  app.listen(process.env.PORT);
  console.log(`Started listening on ${process.env.PORT}`);
};

startServer()
  .catch((err) => console.error(`Error: ${err.message}. Service is shutting down`));
