
const ExchangeRateManager = require('../services/ExchangeRateManager.service.js');

async function performExchange(req, res) {
  console.log(`Request performed with query: ${JSON.stringify(req.query)}`);
  ExchangeRateManager.getExchangeXmlFile()
    .then((fileContent) => {
      console.log('FIle downloaded');
      return ExchangeRateManager.getCubesFromFile(fileContent);
    })
    .then((cubes) => {
      console.log('Cubes object parsed');
      const cubesObject = ExchangeRateManager.parseCubesAndGetCubesObject(cubes);
      console.log(`Cubes object: ${cubesObject.length}`);
      const { amount } = req.query;
      const { srcCurrency } = req.query;
      const { destCurrency } = req.query;
      const { referenceDate } = req.query;
      console.log(`Request performed with amount: ${amount} - srcCurrency: ${srcCurrency} - destCurrency: ${destCurrency} - referenceDate: ${referenceDate}`);
      try {
        console.log('trying to perform  exchange...');
        const result = ExchangeRateManager.performExchange(
          cubesObject,
          amount,
          srcCurrency,
          destCurrency,
          referenceDate,
        );
        console.log(`Exchange performed with result: ${result}`);
        return res.status(200).json({ amount: result, currency: destCurrency });
      } catch (err) {
        console.error(`controller - performExchange - ${err}`);
        return res.status(400).json({ error: err.message });
      }
    })
    .catch((err) => {
      console.error(`Error occurred in perform cexchange: ${err.message}`);
      return res.status(400).json({ error: err.message });
    });
}

module.exports = {
  performExchange,
};
