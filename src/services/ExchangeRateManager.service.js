
/*
Resource to understand how exchange rate work: https://www.thebalance.com/how-to-read-and-calculate-exchange-rates-1978919
*/
const request = require('request');
const { parseString } = require('xml2js');
const fs = require('fs');

function getExchangeXmlFile() {
  const getXmlPromise = new Promise(
    ((resolve, reject) => {
      request(process.env.EXCHANGE_FILE_URL, null, (err, res, body) => {
        if (err) { return reject(err); }
        return resolve(body);
      });
    }),
  );
  return getXmlPromise;
}

function openXmlLocalFile(path) {
  const fileContent = fs.readFileSync(path, 'utf8');
  return fileContent;
}

function getCubesFromFile(xmlFile) {
  const getXmlPromise = new Promise(
    ((resolve, reject) => {
      parseString(xmlFile, (err, result) => {
        if (err) return reject(err);
        const gemes = result['gesmes:Envelope'];
        const cubes = gemes.Cube[0].Cube;
        return resolve(cubes);
      });
    }),
  );
  return getXmlPromise;
}

function parseCubesAndGetCubesObject(cubes) {
  const toReturn = [];
  if (!cubes) return toReturn;

  for (let i = 0; i < cubes.length; i++) {
    const singleCube = {};
    const { time } = cubes[i].$;
    const internalCubes = cubes[i].Cube;
    singleCube.time = time;
    singleCube.cubes = [];
    for (let e = 0; e < internalCubes.length; e++) {
      const TMPSingleCube = internalCubes[e].$;
      const { currency } = TMPSingleCube;
      const { rate } = TMPSingleCube;
      const tmp = {
        currency,
        rate,
      };
      singleCube.cubes.push(tmp);
    }
    toReturn.push(singleCube);
  }

  return toReturn;
}

function getExchangeValueForDateAndCurrency(source, currency, date) {
  if (!source) {
    throw new Error('Exchange rate source not available');
  }
  for (let i = 0; i < source.length; i++) {
    if (source[i].time === date) {
      for (let e = 0; e < source[i].cubes.length; e++) {
        if (source[i].cubes[e].currency === currency) {
          return parseFloat(source[i].cubes[e].rate);
        }
      }
    }
  }
  throw new Error('Currency or date not available');
}


function performExchange(source, amount, inputCurrency, outputCurrency, date) {
  if (!source || !inputCurrency || !outputCurrency || !date) throw new Error('parameter error - some info are null');
  try {
    const inputRate = inputCurrency !== 'EUR' ? getExchangeValueForDateAndCurrency(source, inputCurrency, date) : 1;
    const outputRate = outputCurrency !== 'EUR' ? getExchangeValueForDateAndCurrency(source, outputCurrency, date) : 1;
    const conversionObject = {
      source,
      amount,
      inputCurrency,
      outputCurrency,
      date,
    };

    if (inputRate === outputRate) return conversionObject.amount;

    let result;
    if (inputCurrency === 'EUR' && outputCurrency !== 'EUR') {
      console.log('Preparing to perform exchange between value EUR and !EUR');
      result = performExchangeFromEURtoNotEUR(conversionObject);
    } else if (inputCurrency !== 'EUR' && outputCurrency === 'EUR') {
      console.log('Preparing to perform exchange between value !EUR and EUR');
      result = performExchangeFromNotEURtoEUR(conversionObject);
    } else {
      console.log('Preparing to perform exchange between value åå!EUR and !EUR');
      result = performExchangeFromNotEURtoNotEUR(conversionObject);
    }
    console.log(`Result is ${result}`);

    const finalValue = Math.round((result + Number.EPSILON) * 100) / 100;
    return finalValue;
  } catch (err) {
    console.error(`performExchange - error: ${err.message}`);
    throw err;
  }
}

function performExchangeFromEURtoNotEUR(conversionValue) {
  const outputRate = getExchangeValueForDateAndCurrency(
    conversionValue.source,
    conversionValue.outputCurrency,
    conversionValue.date,
  );
  console.info(`performExchangeFromEURtoNotEUR - rate: ${outputRate}`);
  return conversionValue.amount * outputRate;
}

function performExchangeFromNotEURtoEUR(conversionValue) {
  const inputRate = getExchangeValueForDateAndCurrency(
    conversionValue.source,
    conversionValue.inputCurrency,
    conversionValue.date,
  );
  console.info(`performExchangeFromEURtoNotEUR - rate: ${inputRate}`);
  return conversionValue.amount / inputRate;
}

function performExchangeFromNotEURtoNotEUR(conversionValue) {
  const inputRate = getExchangeValueForDateAndCurrency(
    conversionValue.source,
    conversionValue.inputCurrency,
    conversionValue.date,
  );

  console.info(`performExchangeFromNotEURtoNotEUR - input rate: ${inputRate}`);
  const fromNotEuroToEurValue = conversionValue.amount / inputRate;

  const outputRate = getExchangeValueForDateAndCurrency(
    conversionValue.source,
    conversionValue.outputCurrency,
    conversionValue.date,
  );

  console.info(`performExchangeFromNotEURtoNotEUR - output rate: ${outputRate}`);
  return fromNotEuroToEurValue * outputRate;
}

module.exports = {
  getExchangeXmlFile,
  openXmlLocalFile,
  getCubesFromFile,
  parseCubesAndGetCubesObject,
  getExchangeValueForDateAndCurrency,
  performExchange,
};
