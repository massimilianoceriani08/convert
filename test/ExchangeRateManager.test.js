
const assert = require('assert');

const EXCHANGE_FILE_URL = 'https://www.ecb.europa.eu/stats/eurofxref/eurofxref-hist-90d.xml';
const exchangeService = require('../src/services/ExchangeRateManager.service.js');

const TEST_FILE_NAME = './test/exampleFIle.xml';


beforeEach(async () => {
  delete process.env.EXCHANGE_FILE_URL;
});

describe('Exchange Rate Manager service ', () => {
  describe('getExchangeXmlFile', () => {
    it('Should return the XML file', (done) => {
      process.env.EXCHANGE_FILE_URL = EXCHANGE_FILE_URL;
      exchangeService.getExchangeXmlFile().then(() => {
        done();
      }).catch(() => {
        assert(false);
      });
    });
  });

  describe('Open xml file', () => {
    it('Should open the xml file correctly', () => {
      exchangeService.openXmlLocalFile(TEST_FILE_NAME);
    });
  });

  describe('Parse xml file', () => {
    it('Should open and parse the xml file and returns all the cubes', (done) => {
      const fileContent = exchangeService.openXmlLocalFile(TEST_FILE_NAME);
      exchangeService.getCubesFromFile(fileContent).then((cubes) => {
        assert.equal(cubes.length, 64);
        done();
      });
    });

    it('Should open and parse the xml file and returns all the cubes on javascrib obj', (done) => {
      const fileContent = exchangeService.openXmlLocalFile(TEST_FILE_NAME);
      exchangeService.getCubesFromFile(fileContent).then((cubes) => {
        assert.equal(cubes.length, 64);
        const parsedCubes = exchangeService.parseCubesAndGetCubesObject(cubes);
        for (let i = 0; i < parsedCubes.length; i++) {
          assert.equal(parsedCubes[i].cubes.length, 32);
        }
        done();
      });
    });

    describe('Get rate for currency and date', () => {
      it('Should open and parse the xml file and return the expected rate', (done) => {
        const expectedCurrency = 'USD';
        const expectedDate = '2019-08-21';
        const expectedRate = 1.1104;
        const fileContent = exchangeService.openXmlLocalFile(TEST_FILE_NAME);
        exchangeService.getCubesFromFile(fileContent).then((cubes) => {
          const parsedCubes = exchangeService.parseCubesAndGetCubesObject(cubes);
          const rate = exchangeService.getExchangeValueForDateAndCurrency(
            parsedCubes,
            expectedCurrency,
            expectedDate,
          );
          assert.equal(rate, expectedRate);
          done();
        });
      });

      it('Should open and parse the xml file and return the error because the currency not exists', (done) => {
        const expectedCurrency = 'NOT_VALID';
        const expectedDate = '2019-08-21';
        const fileContent = exchangeService.openXmlLocalFile(TEST_FILE_NAME);
        exchangeService.getCubesFromFile(fileContent).then((cubes) => {
          const parsedCubes = exchangeService.parseCubesAndGetCubesObject(cubes);
          try {
            exchangeService.getExchangeValueForDateAndCurrency(
              parsedCubes,
              expectedCurrency,
              expectedDate,
            );
          } catch (error) {
            assert.equal(error.message, 'Currency or date not available');
            done();
          }
        });
      });

      it('Should return error because soure are not passed as parameter', (done) => {
        const expectedCurrency = 'NOT_VALID';
        const expectedDate = '2019-08-21';
        try {
          exchangeService.getExchangeValueForDateAndCurrency(
            null,
            expectedCurrency,
            expectedDate,
          );
        } catch (error) {
          assert.equal(error.message, 'Exchange rate source not available');
          done();
        }
      });
    });
  });

  describe('performExchange', () => {
    describe('From not EUR to EUR', () => {
      it('Should return the current exchange value for same currency', (done) => {
        const inputCurrency = 'EUR';
        const outputCurrency = 'EUR';
        const inputAmount = 1.89;
        const expectedResult = 1.89;
        const expectedDate = '2019-08-21';
        const fileContent = exchangeService.openXmlLocalFile(TEST_FILE_NAME);
        exchangeService.getCubesFromFile(fileContent).then((cubes) => {
          const parsedCubes = exchangeService.parseCubesAndGetCubesObject(cubes);
          const result = exchangeService.performExchange(
            parsedCubes,
            inputAmount,
            inputCurrency,
            outputCurrency,
            expectedDate,
          );
          assert.equal(result, expectedResult);
          done();
        });
      });
    });

    describe('From not NOT EUR to EUR', () => {
      it('Should return the expected value', (done) => {
        const inputCurrency = 'USD';
        const outputCurrency = 'EUR';
        const inputAmount = 1.11;
        const expectedResult = 1;
        const expectedDate = '2019-08-21';
        const fileContent = exchangeService.openXmlLocalFile(TEST_FILE_NAME);
        exchangeService.getCubesFromFile(fileContent).then((cubes) => {
          const parsedCubes = exchangeService.parseCubesAndGetCubesObject(cubes);
          const result = exchangeService.performExchange(
            parsedCubes,
            inputAmount,
            inputCurrency,
            outputCurrency,
            expectedDate,
          );
          assert.equal(result, expectedResult);
          done();
        });
      });
    });

    describe('From not EUR to not EUR', () => {
      it('Should return the expected value', (done) => {
        const inputCurrency = 'JPY';
        const outputCurrency = 'BGN';
        const inputAmount = 1.0;
        const expectedResult = 0.02;
        const expectedDate = '2019-11-18';
        const fileContent = exchangeService.openXmlLocalFile(TEST_FILE_NAME);
        exchangeService.getCubesFromFile(fileContent).then((cubes) => {
          const parsedCubes = exchangeService.parseCubesAndGetCubesObject(cubes);
          const result = exchangeService.performExchange(
            parsedCubes,
            inputAmount,
            inputCurrency,
            outputCurrency,
            expectedDate,
          );
          assert.equal(result, expectedResult);
          done();
        });
      });
    });

    describe('From not valid currency to not EUR', () => {
      it('Should return the expected value', (done) => {
        const inputCurrency = 'NOTVALID';
        const outputCurrency = 'BGN';
        const inputAmount = 1.0;
        const expectedDate = '2019-11-18';
        const fileContent = exchangeService.openXmlLocalFile(TEST_FILE_NAME);
        exchangeService.getCubesFromFile(fileContent)
          .then((cubes) => {
            const parsedCubes = exchangeService.parseCubesAndGetCubesObject(cubes);
            exchangeService.performExchange(
              parsedCubes,
              inputAmount,
              inputCurrency,
              outputCurrency,
              expectedDate,
            );
            assert(false);
            done();
          })
          .catch(() => {
            assert(true);
            done();
          });
      });
    });
  });
});
