FROM node:10

WORKDIR /usr/src/app

ENV PORT=8080
ENV EXCHANGE_FILE_URL=https://www.ecb.europa.eu/stats/eurofxref/eurofxref-hist-90d.xml

# Install dependencies
COPY package*.json ./
RUN npm install

COPY src ./src/
RUN cd src && ls -all
EXPOSE 8080
CMD [ "npm", "start" ]